package com.firemix.hmsremotconfig.HMSDrive;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.huawei.cloud.base.auth.DriveCredential;
import com.huawei.cloud.services.drive.DriveScopes;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.common.ApiException;
import com.huawei.hms.support.api.entity.auth.Scope;
import com.huawei.hms.support.hwid.HuaweiIdAuthAPIManager;
import com.huawei.hms.support.hwid.HuaweiIdAuthManager;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper;
import com.huawei.hms.support.hwid.result.AuthHuaweiId;
import com.huawei.hms.support.hwid.service.HuaweiIdAuthService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.firemix.hmsremotconfig.HMSDrive.HDriveHelper.HWCredential;

public class HuaweiDriveServices extends AppCompatActivity {

    private static final Map<String, String> MIME_TYPE_MAP = new HashMap<String, String>();
    private static HuaweiDriveServices Servicios;
    private Context context;

    static {
        MIME_TYPE_MAP.put(".doc", "application/msword");
        MIME_TYPE_MAP.put(".jpg", "image/jpeg");
        MIME_TYPE_MAP.put(".mp3", "audio/x-mpeg");
        MIME_TYPE_MAP.put(".mp4", "video/mp4");
        MIME_TYPE_MAP.put(".pdf", "application/pdf");
        MIME_TYPE_MAP.put(".png", "image/png");
        MIME_TYPE_MAP.put(".txt", "text/plain");
        MIME_TYPE_MAP.put(".zip", "application/zip");
        MIME_TYPE_MAP.put(".json", "application/json");
        MIME_TYPE_MAP.put(".aac", "audio/aac");
        MIME_TYPE_MAP.put(".abw", "application/x-abiword");
        MIME_TYPE_MAP.put(".arc", "application/x-freearc");
        MIME_TYPE_MAP.put(".avi", "video/x-msvideo");
        MIME_TYPE_MAP.put(".azw", "application/vnd.amazon.ebook");
        MIME_TYPE_MAP.put(".bin", "application/octet-stream");
        MIME_TYPE_MAP.put(".bmp", "image/bmp");
        MIME_TYPE_MAP.put(".csv", "text/csv");
        MIME_TYPE_MAP.put(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        MIME_TYPE_MAP.put(".epub", "application/epub+zip");
        MIME_TYPE_MAP.put(".gif", "image/gif");
        MIME_TYPE_MAP.put(".html", "text/html");
        MIME_TYPE_MAP.put(".jpeg", "image/jpeg");
        MIME_TYPE_MAP.put(".js", "text/javascript");
        MIME_TYPE_MAP.put(".php", "application/x-httpd-php");
        MIME_TYPE_MAP.put(".ppt", "application/vnd.ms-powerpoint");
        MIME_TYPE_MAP.put(".xls", "application/vnd.ms-excel");
        MIME_TYPE_MAP.put(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        MIME_TYPE_MAP.put(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        MIME_TYPE_MAP.put(".rar", "application/vnd.rar");
        MIME_TYPE_MAP.put(".7z", "application/x-7z-compressed");
    }

    private HuaweiIdAuthService mAuthManager;
    private HuaweiIdAuthParams mAuthParam;
    public DriveCredential mCredential;
    private HWDrivesActions actions;
    private HDriveHelper hDriveHelper;
    private String accessToken;
    private final DriveCredential.AccessMethod refreshAT = new DriveCredential.AccessMethod() {
        @Override
        public String refreshToken() {
            return accessToken;
        }
    };
    private String unionId;

    private HuaweiDriveServices(Activity context, HWDrivesActions hdrivers) {
        this.context = context;
         actions = hdrivers;
        List<Scope> scopeList = new ArrayList<>();
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_FILE));
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_APPDATA));
        scopeList.add(HuaweiIdAuthAPIManager.HUAWEIID_BASE_SCOPE);
        mAuthParam = new HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM)
                .setIdToken()
                .setAccessToken()
                .setScopeList(scopeList)
                .createParams();
        mAuthManager = HuaweiIdAuthManager.getService(context, mAuthParam);
        Task<AuthHuaweiId> task = mAuthManager.silentSignIn();
        task.addOnSuccessListener(authHuaweiId -> {
            accessToken = authHuaweiId.getAccessToken();
            unionId = authHuaweiId.getUnionId();
            DriveCredential.Builder builder = new DriveCredential.Builder(unionId, refreshAT);
            mCredential = builder.build().setAccessToken(accessToken);
            if (mCredential != null) {
                actions.HDriveSession(true, authHuaweiId);
                hDriveHelper = new HDriveHelper(HWCredential(context, mCredential));
            }
        });
        task.addOnFailureListener(e -> {

        });
    }

    public static HuaweiDriveServices getInstance(Activity context, HWDrivesActions hdrivers) {
        if (context != null) {
            Servicios = new HuaweiDriveServices(context, hdrivers);
        }
        return Servicios;
    }

    /*Inicializa la el objeto de tipo HDriveHelper la primera vez para poder usar todos los métodos de esta clase
        Sino se manda a llamar no podrás usarlos ya que te mandará el error 404 que se refiere a que aun no existe
        una instancia creada para usar la API de Huawei Drive
     */
    public void LoginSession(){
        List<Scope> scopeList = new ArrayList<>();
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_FILE));
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_APPDATA));
        scopeList.add(HuaweiIdAuthAPIManager.HUAWEIID_BASE_SCOPE);
        mAuthParam = new HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM)
                .setIdToken()
                .setAccessToken()
                .setScopeList(scopeList)
                .createParams();
        mAuthManager = HuaweiIdAuthManager.getService(context, mAuthParam);
        Task<AuthHuaweiId> task = mAuthManager.silentSignIn();
        task.addOnSuccessListener(authHuaweiId -> {
            accessToken = authHuaweiId.getAccessToken();
            unionId = authHuaweiId.getUnionId();
            DriveCredential.Builder builder = new DriveCredential.Builder(unionId, refreshAT);
            mCredential = builder.build().setAccessToken(accessToken);
            if (mCredential != null) {
                hDriveHelper = new HDriveHelper(HWCredential(context, mCredential));
            }
        });
        task.addOnFailureListener(e -> {

        });
    }

    //Crea un nuevo folder en la carpeta raíz de Huawei Drive
    public void CreateFolder(String nameFolder) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.createFolder(nameFolder, null)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONObject json_data = new JSONObject(gson.toJson(HDriveHolders));
                            if (!json_data.getString("id").equals("")) {
                                actions.FolderCreate(json_data.getString("id"), nameFolder);
                            }
                        } else {
                            actions.ErrorHDrive("Hubo un error al crear el folder " + nameFolder);
                        }
                    } catch (JSONException e) {
                        actions.ErrorHDrive("Error: " + e.getMessage());
                    }
                })
                .addOnFailureListener(e -> actions.ErrorHDrive(e.getMessage()));
    }

    //Buscar el folder solicitado en Huawei Drive
    public void SearchFolder(String nameFolder) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFolder(nameFolder)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                actions.FolderSearch(json_data.getString("id"), nameFolder);
                            }
                        } else {
                            actions.FolderSearch("", nameFolder);
                        }
                    } catch (JSONException e) {
                        actions.ErrorHDrive(e.getMessage());
                    }
                })
                .addOnFailureListener(e -> actions.ErrorHDrive(e.getMessage()));
    }

    //Crea un folder si es que este no existe
    public void CreateFolderIfNotExist(String nameFolder) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFolder(nameFolder)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                actions.ErrorHDrive("El folder solicitado ya existe y no puede ser creado");
                            }
                        } else {
                            CreateFolder(nameFolder);
                        }
                    } catch (JSONException e) {
                        actions.ErrorHDrive(e.getMessage());
                    }
                })
                .addOnFailureListener(e -> actions.ErrorHDrive(e.getMessage()));
    }

    //Crea un folder dentro de otro
    public void CreateSubFolder(String nameFolder, String FolderId) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.createFolder(nameFolder, FolderId)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONObject json_data = new JSONObject(gson.toJson(HDriveHolders));
                            if (!json_data.getString("id").equals("")) {
                                actions.SubFolderCreate(json_data.getString("id"), nameFolder);
                            }
                        } else {
                            actions.ErrorHDrive("El folder no ha podido ser creado");
                        }
                    } catch (JSONException e) {
                        actions.ErrorHDrive("Error: " + e.getMessage());
                    }
                })
                .addOnFailureListener(e -> actions.ErrorHDrive(e.getMessage()));
    }

    //Sube un archivo sino existe
    public void UploadFileItNotExist(File archivo, String FolderId){
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFile(archivo.getName(), mimeType(archivo))
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                actions.ErrorHDrive("El archivo "+archivo.getName()+ " ya existe en huawei drive");
                            }
                        } else {
                            UploadFile(archivo, FolderId);
                        }
                    } catch (JSONException e) {
                        actions.ErrorHDrive("Error: " + e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorHDrive(e.getMessage());
                });
    }

    //Sube un archivo en una carpeta especificada por el FolderId
    public void UploadFile(File archivo, String FolderId) {
        if (hDriveHelper == null) {
            return;
        }
        try {
            hDriveHelper.uploadFile(archivo, FolderId)
                    .addOnSuccessListener(HdriveHolders -> actions.SuccessUpload(true))
                    .addOnFailureListener(e -> {
                        actions.SuccessUpload(false);
                        actions.ErrorHDrive(e.getMessage());
                    });
        } catch (Exception e) {
            actions.ErrorHDrive(e.getMessage());
        }
    }

    //Sube un archivo en una carpeta especificada por el FolderId
    public void UploadFiles(File archivo, String FolderId) {
        if (hDriveHelper == null) {
            return;
        }
        try {
            hDriveHelper.uploadFile(archivo, FolderId)
                    .addOnSuccessListener(HdriveHolders -> {})
                    .addOnFailureListener(e -> {
                        actions.SuccessUpload(false);
                        actions.ErrorHDrive(e.getMessage());
                    });
        } catch (Exception e) {
            actions.ErrorHDrive(e.getMessage());
        }
    }

    //Sube varios archivos en una carpeta especificada por el FolderId sino existen
    public void UploadMultiplesFilesIfNotExist(List<File> archivo, String FolderId) {
        if (hDriveHelper == null) {
            return;
        }
        final boolean[] correct = {true};
        int i = 1;
        for(File archivos : archivo) {
            int finalI = i;
            hDriveHelper.searchFile(archivos.getName(), mimeType(archivos))
                    .addOnSuccessListener(HDriveHolders -> {
                        Gson gson = new Gson();
                        if (gson.toJson(HDriveHolders).equals("[]")) {
                            UploadFiles(archivos, FolderId);
                        }
                        actions.UploadProgress(finalI * 100 / archivo.size(),finalI,archivo.size());
                    })
                    .addOnFailureListener(e -> {
                        correct[0] = false;
                        actions.ErrorHDrive(e.getMessage());
                    });

            if(!correct[0]){
                actions.ErrorHDrive("Hubo un error al subir los archivos");
                break;
            }
            i++;
        }
    }

    //Sube varios archivos en una carpeta especificada por el FolderId
    public void UploadMultiplesFiles(List<File> archivo, String FolderId) {
        if (hDriveHelper == null) {
            return;
        }
        try {
            final boolean[] correct = {true};
            int i = 1;
            for(File archivos : archivo){
                int finalI = i;
                hDriveHelper.uploadFile(archivos, FolderId)
                        .addOnSuccessListener(HdriveHolders ->  {
                            actions.UploadProgress(finalI * 100 / archivo.size(),finalI,archivo.size());})
                        .addOnFailureListener(e -> {
                            actions.ErrorHDrive(e.getMessage());
                            correct[0] = false;
                        });

                if(!correct[0]){
                    actions.ErrorHDrive("Hubo un error al subir los archivos");
                    break;
                }
                i++;
            }
        } catch (Exception e) {
            actions.ErrorHDrive(e.getMessage());
        }
    }

    //Lista todos los archivos de un folder por su ID
    public void ListFolder(String folderId) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.queryFiles(folderId)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            actions.ListFolder(jArray);
                        } else{
                            actions.ListFolder(new JSONArray());
                        }
                    } catch (JSONException e) {
                        actions.ErrorHDrive("Error: " + e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorHDrive("Error: " + e.getMessage());
                });
    }

    /*Lista todos los archivos de huawei drive, dependiendo de los permisos tenga permitido es lo que podrá ver.
        Si en los permisos del Scope agregas SCOPE_DRIVE podrás ver todos los archivos que tienes en tu huawei drive
        sin importar que hayan sido o no creados por esta app, caso contrario si solo usas
        SCOPE_DRIVE_APPDATA solo verás los que se hayan creado usando esta app
    */
    public void ListAllFiles() {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.queryFiles()
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            actions.ListAllFiles(jArray);
                        } else{
                            actions.ListFolder(new JSONArray());
                        }
                    } catch (JSONException e) {
                        actions.ErrorHDrive("Error: " + e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorHDrive("Error: " + e.getMessage());
                });
    }

    //Elimina un archivo por su ID
    public void DeleteFile(String FileId,String fileName) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.deleteFolderFile(FileId).addOnSuccessListener(aVoid -> {
            actions.DeleteSuccess(true,fileName);
        }).addOnFailureListener(e -> {
            actions.ErrorHDrive(e.getMessage());
        });
    }

    //Descargar un archivo por su ID
    public void DownloadFile(String FileId, File fileName, String Name) {
        if (hDriveHelper == null) {
            return;
        }
        if (fileName.exists()) {
            fileName.delete();
        }
        hDriveHelper.downloadFile(fileName, FileId)
                .addOnSuccessListener(aVoid -> actions.DownloadSuccess(true,Name))
                .addOnFailureListener(e -> {
                    actions.ErrorHDrive(e.getMessage());
                });
    }

    //Descarga múltiples archivos de una lista de IDS
    public void DownloadMultiplesFiles(ArrayList<FilesModel> filesModels){
        if (hDriveHelper == null) {
            return;
        }
        final boolean[] correct = {true};
        int i = 1;
        if(filesModels.size() > 0){
            for(FilesModel archivos : filesModels){
                int finalI = i;
                hDriveHelper.downloadFile(archivos.getArchivo(), archivos.getFileId())
                        .addOnSuccessListener(aVoid -> actions.MultipleProgress(finalI * 100 / filesModels.size(),finalI,filesModels.size(),true))
                        .addOnFailureListener(e -> {
                            actions.ErrorHDrive(e.getMessage());
                            correct[0] = false;
                        });
                if(!correct[0]){
                    actions.ErrorHDrive("Hubo un error al descargar los archivos");
                    break;
                }
                i++;
            }
        }

    }

    //Elimina múltiples archivos de una lista de IDS
    public void DeleteMultiplesFiles(ArrayList<FilesModel> filesModels){
        if (hDriveHelper == null) {
            return;
        }
        final boolean[] correct = {true};
        int i = 1;
        if(filesModels.size() > 0){
            for(FilesModel archivos : filesModels){
                int finalI = i;
                hDriveHelper.deleteFolderFile(archivos.getFileId())
                        .addOnSuccessListener(aVoid -> actions.MultipleProgress(finalI * 100 / filesModels.size(),finalI,filesModels.size(),false))
                        .addOnFailureListener(e -> {
                            actions.ErrorHDrive(e.getMessage());
                            correct[0] = false;
                        });
                if(!correct[0]){
                    actions.ErrorHDrive("Hubo un error al eliminar los archivos");
                    break;
                }
                i++;
            }
        }

    }

    //Cerrar sesión actual del usuario
    public void LogoutHW() {
        if (mAuthManager != null) {
            Task<Void> signOutTask = mAuthManager.signOut();
            signOutTask.addOnSuccessListener(aVoid -> DeclinePermission()).addOnFailureListener(e -> actions.ErrorHDrive(e.getMessage()));
        } else {
            actions.ErrorHDrive("La sesión aun no se ha cargado");
        }
    }

    //Elimina todos los permisos concedidos por el usuario para que al iniciar nuevamente vea los permisos que se le solicitarán
    private void DeclinePermission() {
        mAuthManager.cancelAuthorization().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                actions.HDCloseSession();
            } else {
                Exception exception = task.getException();
                if (exception instanceof ApiException) {
                    actions.ErrorHDrive(exception.getMessage());
                }
            }
        });
    }

    private String mimeType(File file) {
        if (file != null && file.exists() && file.getName().contains(".")) {
            String fileName = file.getName();
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if (MIME_TYPE_MAP.containsKey(suffix)) {
                return MIME_TYPE_MAP.get(suffix);
            }
        }
        return "*/*";
    }

    public interface HWDrivesActions {
        void HDriveSession(boolean login, AuthHuaweiId authHuaweiId);

        void HDCloseSession();

        void ErrorHDrive(String Message);

        void FolderCreate(String FolderId, String folder);

        void SubFolderCreate(String FolderId, String folder);

        void FolderSearch(String FolderId, String folder);

        void SuccessUpload(boolean upload);

        void UploadProgress(int progress,int count,int total);

        void ListFolder(JSONArray response);

        void ListAllFiles(JSONArray response);

        void DeleteSuccess(boolean state, String file);

        void DownloadSuccess(boolean state, String file);

        void MultipleProgress(int progress,int count,int total,boolean type);
    }
}
