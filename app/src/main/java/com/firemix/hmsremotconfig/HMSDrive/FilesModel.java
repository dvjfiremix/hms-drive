package com.firemix.hmsremotconfig.HMSDrive;

import java.io.File;

public class FilesModel {
    String Filename;
    String FileId;
    String Filetype;
    boolean Select;
    int position;
    File archivo;

    public FilesModel(){
        this.Filename = "";
        this.FileId  ="";
        this.Filetype = "";
        this.Select = false;
        this.position = 0;
        this.archivo = null;
    }

    public String getFilename() {
        return Filename;
    }

    public void setFilename(String filename) {
        Filename = filename;
    }

    public String getFileId() {
        return FileId;
    }

    public void setFileId(String fileId) {
        FileId = fileId;
    }

    public String getFiletype() {
        return Filetype;
    }

    public void setFiletype(String filetype) {
        Filetype = filetype;
    }

    public boolean isSelect() {
        return Select;
    }

    public void setSelect(boolean select) {
        Select = select;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }
}
