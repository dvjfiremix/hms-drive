package com.firemix.hmsremotconfig;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.firemix.hmsremotconfig.Configs.HMSRemoteConfig;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_app);
        HMSRemoteConfig.getInstance(null);
        esperar(2000);
    }

    public void esperar(long time) {
        new Handler(getMainLooper()).postDelayed(() -> {
            startActivity(new Intent(MainActivity.this, ejemplo.class));
            finish();
        }, time);
    }

}