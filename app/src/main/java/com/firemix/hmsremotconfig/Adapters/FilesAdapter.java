package com.firemix.hmsremotconfig.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.firemix.hmsremotconfig.Configs.ImagesUtils;
import com.firemix.hmsremotconfig.HMSDrive.FilesModel;
import com.firemix.hmsremotconfig.R;

import java.util.ArrayList;

import static com.firemix.hmsremotconfig.HMSDrive.HDriveHelper.MIMETYPE_FOLDER;

public class FilesAdapter extends RecyclerView.Adapter<FilesHolder> {

    private final Context context;
    private final ArrayList<FilesModel> list;
    private FilesAction action;
    private ImagesUtils imagesUtils;
    private boolean select = false;

    public FilesAdapter(Context context, ArrayList<FilesModel> list, FilesAction actions) {
        this.context = context;
        this.list = list;
        this.action = actions;
        imagesUtils = ImagesUtils.getInstance(context);
    }

    @Override
    public FilesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.design_files, parent, false);
        return new FilesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilesHolder holder, int position) {
        FilesModel data = list.get(position);
        String[] filesTypeVideo = {"mp4","mov","avi","3gp"};
        String[] filesTypeAudio = {"mp3","wav","ogg"};
        String[] filesTypeImage = {"jpg","png","jpeg","bmp","gif"};
        String[] filesTypeDocument = {"doc","docx","ppt","pptx","txt","xls","xlsx","XLSX","DOCX"};
        String[] filesTypeCompress = {"zip","rar","7zip"};

            for (String tipo : filesTypeVideo) {
                if (data.getFilename().contains(tipo)) {
                    imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_video);
                }
            }

            for (String tipo : filesTypeAudio) {
                if (data.getFilename().contains(tipo)) {
                    imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_speaker);
                }
            }

            for (String tipo : filesTypeImage) {
                if (data.getFilename().contains(tipo)) {
                    imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_image_gallery);
                }
            }

            for (String tipo : filesTypeDocument) {
                if (data.getFilename().contains(tipo)) {
                    imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_text_format);
                }
            }

            for (String tipo : filesTypeCompress) {
                if (data.getFilename().contains(tipo)) {
                    imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_zip);
                }
            }

            if (data.getFilename().contains("pdf")) {
                imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_pdf);
            }

        if (data.getFilename().contains("apk")) {
            imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_android);
        }

        if (data.getFiletype().equals(MIMETYPE_FOLDER)) {
            imagesUtils.LoadImageResources(holder.iconFile, R.drawable.ic_folder);
            holder.DownloadFile.setVisibility(View.GONE);
        } else{
            holder.DownloadFile.setVisibility(View.VISIBLE);
        }

        if(data.isSelect()){
            holder.select_files.setVisibility(View.VISIBLE);
        } else{
            holder.select_files.setVisibility(View.GONE);
        }

            holder.nameFile.setText(data.getFilename());
            holder.idFile.setText("ID: " + data.getFileId());
            holder.DownloadFile.setOnClickListener(v -> action.Download(data.getFilename(), data.getFileId(), position));
            holder.DeleteFile.setOnClickListener(v -> action.DeleteFile(data.getFilename(), data.getFileId(), position));
            holder.itemView.setOnLongClickListener(v -> {
                if(data.getFiletype().equals(MIMETYPE_FOLDER)){
                    action.MessageAction("Los folders no están permitidos para selección múltiple");
                } else{
                    data.setSelect(true);
                    action.SelectMultiplesFiles(data);
                    select = true;
                }
                return true;
            });

            holder.itemView.setOnClickListener(v -> {
                if(select) {
                    if(!data.getFiletype().equals(MIMETYPE_FOLDER)) {
                        if (data.isSelect()) {
                            data.setSelect(false);
                            action.RemoveMultiplesFiles(data);
                        } else {
                            data.setSelect(true);
                            action.SelectMultiplesFiles(data);
                        }
                    } else{
                        action.MessageAction("Los folders no están permitidos para selección múltiple");
                    }
                }
            });

    }

    public void ReloadSelect(){
        select = false;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface FilesAction{
        void DeleteFile(String name, String FileId,int position);
        void Download(String name, String FileId, int position);
        void SelectMultiplesFiles(FilesModel filesModel);
        void RemoveMultiplesFiles(FilesModel filesModel);
        void MessageAction(String message);
    }
}
