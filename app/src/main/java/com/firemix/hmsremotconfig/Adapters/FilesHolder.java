package com.firemix.hmsremotconfig.Adapters;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.firemix.hmsremotconfig.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilesHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iconType)
    public ImageView iconFile;
    @BindView(R.id.nameFile)
    public TextView nameFile;
    @BindView(R.id.fileId)
    public TextView idFile;
    @BindView(R.id.Delete)
    public LinearLayout DeleteFile;
    @BindView(R.id.Download)
    public LinearLayout DownloadFile;
    @BindView(R.id.SelectFiles)
    public FrameLayout select_files;

    public FilesHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
