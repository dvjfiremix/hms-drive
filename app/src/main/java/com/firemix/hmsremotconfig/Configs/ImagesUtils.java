package com.firemix.hmsremotconfig.Configs;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firemix.hmsremotconfig.R;

public class ImagesUtils {
    private static ImagesUtils utils;
    private final Context ctx;

    private ImagesUtils(Context context) {
        this.ctx = context;
    }

    public static ImagesUtils getInstance(Context context) {
        return new ImagesUtils(context);
    }

    //Carga una imagen en el CircleImageView por una de las imagenes de la app
    public void LoadImageResources(ImageView imagen, int photo) {
        try {
            Glide.with(ctx)
                    .load(photo)
                    .placeholder(R.drawable.ic_huawei)
                    .fitCenter()
                    .into(imagen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Carga una imagen en el CircleImageView por una de las imagenes de la app
    public void LoadImagePath(ImageView imagen, String photo) {
        try {
            Glide.with(ctx)
                    .load(photo)
                    .placeholder(R.drawable.ic_huawei)
                    .fitCenter()
                    .into(imagen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
