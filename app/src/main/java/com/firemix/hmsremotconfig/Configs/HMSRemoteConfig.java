package com.firemix.hmsremotconfig.Configs;

import com.huawei.agconnect.remoteconfig.AGConnectConfig;
import com.huawei.agconnect.remoteconfig.ConfigValues;

public class HMSRemoteConfig {
    private static HMSRemoteConfig hmsRemotConfig;
    private final HMSActions actions;

    private HMSRemoteConfig(HMSActions actions) {
        this.actions = actions;
        AGConnectConfig config = AGConnectConfig.getInstance();
        // Habilitamos esta clase para decirle que nos carge lo último que guardó y lo aplicamos
        ConfigValues last = config.loadLastFetched();
        config.apply(last);
        // Si están en pruebas dejen este valor dentro de fecth en 0 si lo mandan en producción es recomendable
        // dejarlo para cada 24 HRS

        //los nombres que están entre comillas deben ser los mismo tal cual lo pusieron en Remote Configuration de Huawei
        config.fetch(0).addOnSuccessListener(configValues -> {
            config.apply(configValues);
            configValues.getValueAsLong("version");
            configValues.getValueAsString("Titulo");
            configValues.getValueAsString("Mensaje");
            configValues.getValueAsBoolean("Estado");
        }).addOnFailureListener(Throwable::printStackTrace);
    }

    public static HMSRemoteConfig getInstance(HMSActions actions) {
        return hmsRemotConfig = new HMSRemoteConfig(actions);
    }

    //Este método lo cree para poder usar con la interfaz que verificará si existe una nueva versión en la app
    // cada vez que la abran.
    public void Comparar() {
        actions.CheckVersion();
    }

    // Interface usada para notificar cambios con los valores obtenidos de Remote Confugration
    public interface HMSActions {
        void CheckVersion();
    }
}
