package com.firemix.hmsremotconfig.Configs;

public class RemoteData {
    public long Version;
    public String Titulo;
    public String Mensaje;
    public boolean Estado;

    public RemoteData() {
        this.Version = 1;
        this.Titulo = "";
        this.Mensaje = "";
        this.Estado = false;
    }

    public long getVersion() {
        return Version;
    }

    public void setVersion(long version) {
        Version = version;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }
}
