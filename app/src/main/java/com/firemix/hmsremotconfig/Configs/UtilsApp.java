package com.firemix.hmsremotconfig.Configs;

import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;

import com.huawei.agconnect.remoteconfig.AGConnectConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class UtilsApp {
    private static UtilsApp utils;

    private UtilsApp() {

    }

    public static UtilsApp getInstance() {
        return new UtilsApp();
    }

    // Los valores dentro de las comillas deben ser los mismos de Remote Config y deben ver que sea del mismo tipo
    // Esta es un método que regresa una clase con los valores traidos desde Remote Configuration
    public RemoteData obtenerDatos() {
        AGConnectConfig config = AGConnectConfig.getInstance();
        Map<String, Object> map = config.getMergedAll();
        JSONObject jsonObject = new JSONObject(map);
        RemoteData remoteData = new RemoteData();
        try {
            remoteData.setVersion(jsonObject.getLong("version"));
            remoteData.setTitulo(jsonObject.getString("Titulo"));
            remoteData.setMensaje(jsonObject.getString("Mensaje"));
            remoteData.setEstado(jsonObject.getBoolean("Estado"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return remoteData;
    }
}
