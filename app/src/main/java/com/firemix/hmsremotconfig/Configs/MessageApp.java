package com.firemix.hmsremotconfig.Configs;

import android.content.Context;
import android.widget.Toast;

import com.firemix.hmsremotconfig.R;
import com.github.javiersantos.bottomdialogs.BottomDialog;

public class MessageApp {
    private static MessageApp messageApp;
    private final MessageActions actions;
    private final Context ctx;

    private MessageApp(Context ctx, MessageActions actions) {
        this.ctx = ctx;
        this.actions = actions;
    }

    public static MessageApp getInstance(Context ctx, MessageActions actions) {
        if (ctx != null) {
            messageApp = new MessageApp(ctx, actions);
        }
        return messageApp;
    }

    public void LoadMessage(String Title, String Msj, boolean state) {
        new BottomDialog.Builder(ctx)
                .setTitle(Title)
                .setContent(Msj)
                .setPositiveText("SI")
                .setNegativeText("NO")
                .setCancelable(false)
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .onPositive(dialog -> {
                    if (state) {
                        actions.Accept(1);
                    } else {
                        actions.Accept(0);
                    }
                    dialog.dismiss();
                })
                .setNegativeTextColorResource(R.color.black)
                .onNegative(dialog -> actions.Cancel())
                .show();
    }

    public void DeleteFile(String Title, String Msj) {
        new BottomDialog.Builder(ctx)
                .setTitle(Title)
                .setContent(Msj)
                .setPositiveText("SI")
                .setNegativeText("NO")
                .setCancelable(false)
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .onPositive(dialog -> {
                  actions.Accept(2);
                    dialog.dismiss();
                })
                .setNegativeTextColorResource(R.color.black)
                .onNegative(dialog -> actions.Cancel())
                .show();
    }

    public void DownloadFile(String Title, String Msj) {
        new BottomDialog.Builder(ctx)
                .setTitle(Title)
                .setContent(Msj)
                .setPositiveText("SI")
                .setNegativeText("NO")
                .setCancelable(false)
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .onPositive(dialog -> {
                    actions.Accept(3);
                    dialog.dismiss();
                })
                .setNegativeTextColorResource(R.color.black)
                .onNegative(dialog -> actions.Cancel())
                .show();
    }

    public void DeleteMultipleFile(String Title, String Msj) {
        new BottomDialog.Builder(ctx)
                .setTitle(Title)
                .setContent(Msj)
                .setPositiveText("SI")
                .setNegativeText("NO")
                .setCancelable(false)
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .onPositive(dialog -> {
                    actions.Accept(4);
                    dialog.dismiss();
                })
                .setNegativeTextColorResource(R.color.black)
                .onNegative(dialog -> actions.Cancel())
                .show();
    }

    public void DownloadMultipleFile(String Title, String Msj) {
        new BottomDialog.Builder(ctx)
                .setTitle(Title)
                .setContent(Msj)
                .setPositiveText("SI")
                .setNegativeText("NO")
                .setCancelable(false)
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .onPositive(dialog -> {
                    actions.Accept(5);
                    dialog.dismiss();
                })
                .setNegativeTextColorResource(R.color.black)
                .onNegative(dialog -> actions.Cancel())
                .show();
    }

    public void LoadToast(String mensaje) {
        Toast.makeText(ctx, mensaje, Toast.LENGTH_LONG).show();
    }

    public interface MessageActions {
        void Accept(int op);

        void Cancel();
    }
}
