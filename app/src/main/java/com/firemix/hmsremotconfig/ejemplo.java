package com.firemix.hmsremotconfig;

import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firemix.hmsremotconfig.Adapters.FilesAdapter;
import com.firemix.hmsremotconfig.HMSDrive.FilesModel;
import com.firemix.hmsremotconfig.Configs.FileUtils;
import com.firemix.hmsremotconfig.Configs.HMSRemoteConfig;
import com.firemix.hmsremotconfig.Configs.ImagesUtils;
import com.firemix.hmsremotconfig.Configs.MessageApp;
import com.firemix.hmsremotconfig.Configs.RemoteData;
import com.firemix.hmsremotconfig.Configs.SharedData;
import com.firemix.hmsremotconfig.Configs.UtilsApp;
import com.firemix.hmsremotconfig.HMSDrive.HuaweiDriveServices;
import com.huawei.cloud.base.util.StringUtils;
import com.huawei.cloud.client.exception.DriveCode;
import com.huawei.cloud.services.drive.DriveScopes;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.support.api.entity.auth.Scope;
import com.huawei.hms.support.hwid.HuaweiIdAuthAPIManager;
import com.huawei.hms.support.hwid.HuaweiIdAuthManager;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper;
import com.huawei.hms.support.hwid.result.AuthHuaweiId;
import com.huawei.hms.support.hwid.service.HuaweiIdAuthService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.firemix.hmsremotconfig.Configs.Globals.LAST_FOLDER_ID;

public class ejemplo extends AppCompatActivity implements MessageApp.MessageActions, HMSRemoteConfig.HMSActions, View.OnClickListener, HuaweiDriveServices.HWDrivesActions, FilesAdapter.FilesAction {
    //Identificador de activity Result
    private static final int REQUEST_SIGN_IN_LOGIN = 1645;
    public static final int PERMISSION_REQUEST_CODE = 1500;
    public static final int REQUEST_SELECT_FILE = 200;
    public static final int REQUEST_SELECT_FILES = 300;

    //Views
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.infoUser)
    LinearLayout infoUser;
    @BindView(R.id.foto)
    ImageView foto;
    @BindView(R.id.userT)
    TextView username;
    @BindView(R.id.close)
    Button logout;
    @BindView(R.id.allFunctions)
    LinearLayout functions;
    @BindView(R.id.op1)
    Button folderNew;
    @BindView(R.id.op2)
    Button subfolderNew;
    @BindView(R.id.op3)
    Button filesOption;

    //Create Folder
    @BindView(R.id.createForm)
    LinearLayout createForm;
    @BindView(R.id.nameFolder)
    EditText nameFolder;
    @BindView(R.id.create)
    Button createFolder;
    @BindView(R.id.backFolder)
    Button backFolder;

    //Create SubFolder
    @BindView(R.id.createSubform)
    LinearLayout createSubForm;
    @BindView(R.id.nameSearchFolder)
    EditText nameSearchFolder;
    @BindView(R.id.folderIdSearch)
    TextView idSearchFolder;
    @BindView(R.id.searchFolder)
    LinearLayout searchFolder;
    @BindView(R.id.foundData)
    LinearLayout foundDataFolder;
    @BindView(R.id.subfolderName)
    EditText subFolderName;
    @BindView(R.id.createSub)
    Button createSubFolder;
    @BindView(R.id.backSub)
    Button backSubfolder;

    //FilesOptions
    @BindView(R.id.filesOption)
    LinearLayout filesForm;
    @BindView(R.id.selectFile)
    EditText selectFile;
    @BindView(R.id.uploadFile)
    LinearLayout uploadFile;
    @BindView(R.id.selectMultipleFile)
    EditText selectMultipleFile;
    @BindView(R.id.uploadMultipleFile)
    LinearLayout uploadMultipleFile;
    @BindView(R.id.listFiles)
    Button listFiles;
    @BindView(R.id.backFiles)
    Button backFiles;
    @BindView(R.id.multiples)
    LinearLayout multiples;
    @BindView(R.id.Delete)
    LinearLayout DeleteMultiples;
    @BindView(R.id.Download)
    LinearLayout DownloadMultiples;
    @BindView(R.id.progressContentMultiple)
    LinearLayout progressContentMultiple;
    @BindView(R.id.texto_info_multiple)
    TextView textInfoMultiple;
    @BindView(R.id.progreso_multiple)
    ProgressBar progreso_multiple;
    @BindView(R.id.cargas)
    ProgressBar cargas;
    @BindView(R.id.recyclerFiles)
    RecyclerView recycler;
    FilesAdapter adapter;
    ArrayList<FilesModel> files = new ArrayList<>();
    private ArrayList<FilesModel> seleccionados = new ArrayList<>();
    ArrayList<FilesModel> multis = new ArrayList<>();

    //ProgressContent
    @BindView(R.id.progressContent)
    LinearLayout progressContent;
    @BindView(R.id.progreso)
    ProgressBar progreso;
    @BindView(R.id.texto_info)
    TextView textoInfo;

    //HMS RemoteConfiguration
    private HMSRemoteConfig remoteConfig;
    private UtilsApp utilsApp;
    private MessageApp messageApp;
    private ImagesUtils imagesUtils;
    private HuaweiDriveServices hwService;
    private long actual;
    private String FolderId = "";
    private String FolderIdSearch = "";
    private File archivo;
    private List<File> archivos = new ArrayList<>();
    private int posicionDelete = 0;
    private String FileIdDelete = "";
    private String FileDeleteName = "";
    private String FileIdDownload = "";
    private String FileDownloadName = "";

    //SharedData
    SharedData sharedData;
    //Drive Kit
    private String accessToken;
    private String unionId;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        messageApp = MessageApp.getInstance(this, this);
        utilsApp = UtilsApp.getInstance();
        remoteConfig = HMSRemoteConfig.getInstance(this);
        imagesUtils = ImagesUtils.getInstance(this);
        hwService = HuaweiDriveServices.getInstance(this, this);
        sharedData = SharedData.getInstance(this);
        FolderId = sharedData.getStringData(LAST_FOLDER_ID);
        adapter = new FilesAdapter(this,files,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(linearLayoutManager);
        recycler.setAdapter(adapter);

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            actual = packageInfo.getLongVersionCode();
            remoteConfig.Comparar();
        } catch (Exception e) {

        }

        login.setOnClickListener(this);
        logout.setOnClickListener(this);
        folderNew.setOnClickListener(this);
        createFolder.setOnClickListener(this);
        backFolder.setOnClickListener(this);
        subfolderNew.setOnClickListener(this);
        filesOption.setOnClickListener(this);
        createSubFolder.setOnClickListener(this);
        backSubfolder.setOnClickListener(this);
        searchFolder.setOnClickListener(this);
        selectFile.setOnClickListener(this);
        uploadFile.setOnClickListener(this);
        listFiles.setOnClickListener(this);
        backFiles.setOnClickListener(this);
        selectMultipleFile.setOnClickListener(this);
        uploadMultipleFile.setOnClickListener(this);
        DeleteMultiples.setOnClickListener(this);
        DownloadMultiples.setOnClickListener(this);
    }

    private void IniciarSesion() {
        List<Scope> scopeList = new ArrayList<>();
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_FILE));
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_APPDATA));
        scopeList.add(HuaweiIdAuthAPIManager.HUAWEIID_BASE_SCOPE);
        HuaweiIdAuthParams mAuthParam = new HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM)
                .setIdToken()
                .setAccessToken()
                .setScopeList(scopeList)
                .createParams();
        HuaweiIdAuthService mAuthManager = HuaweiIdAuthManager.getService(this, mAuthParam);
        startActivityForResult(mAuthManager.getSignInIntent(), REQUEST_SIGN_IN_LOGIN);
    }

    private void CerrarSesion() {
        hwService.LogoutHW();
    }

    private void ShowForms(int estado) {
        functions.setVisibility(estado);
    }

    private void ShowOtherForms(int option) {
        if (option == 0) {
            createForm.setVisibility(View.VISIBLE);
            showOptions(false);
        } else if (option == 1) {
            createSubForm.setVisibility(View.VISIBLE);
            foundDataFolder.setVisibility(View.GONE);
            showOptions(false);
        } else if (option == 2) {
            filesForm.setVisibility(View.VISIBLE);
            filesOption.setVisibility(View.GONE);
            showOptions(false);
        } else {
            showOptions(true);
        }
    }

    private void showOptions(boolean show) {
        if (show) {
            folderNew.setVisibility(View.VISIBLE);
            subfolderNew.setVisibility(View.VISIBLE);
            filesOption.setVisibility(View.VISIBLE);
            createForm.setVisibility(View.GONE);
            createSubForm.setVisibility(View.GONE);
            filesForm.setVisibility(View.GONE);
            progressContentMultiple.setVisibility(View.GONE);
            multiples.setVisibility(View.GONE);
            files.clear();
            adapter.notifyDataSetChanged();
        } else {
            folderNew.setVisibility(View.GONE);
            subfolderNew.setVisibility(View.GONE);
            filesOption.setVisibility(View.GONE);
            idSearchFolder.setText("Esperando encontrar un folder");
            selectFile.setText("");
            selectMultipleFile.setText("");
            subFolderName.setText("");
            nameSearchFolder.setText("");
        }
    }

    private void OpenFiles() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("*/*");
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        startActivityForResult(i, REQUEST_SELECT_FILE);
    }

    private void OpenFilesMultiple() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("*/*");
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(i,"Seleccionar archivos"), REQUEST_SELECT_FILES);
    }

    public int init(String unionID, String at) {
        if (StringUtils.isNullOrEmpty(unionID) || StringUtils.isNullOrEmpty(at)) {
            return DriveCode.ERROR;
        }
        return DriveCode.SUCCESS;
    }

    //Permisos
    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean WriteExt = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean ReadExt = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                if (WriteExt && ReadExt) {
                    OpenFiles();
                } else {
                    messageApp.LoadToast("Para poder cargar los archivos es necesario aceptar los permisos");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                            RequestPermission("Los permisos de lectura y escritura de archivos son necesarios para realizar esta función",
                                    (dialog, which) -> requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE},
                                            PERMISSION_REQUEST_CODE));
                        }
                    }

                }
            }
        }
    }

    public void RequestPermission(String msj, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(msj)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancelar", null)
                .create()
                .show();
    }

    @Override
    public void Accept(int op) {
        if (op == 1) {
            finish();
        }  else if (op == 2) {
            hwService.DeleteFile(FileIdDelete,FileDeleteName);
        } else if (op == 3) {
            File Download = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(),  FileDownloadName);
            messageApp.LoadToast("Descargando "+FileDownloadName + " espere por favor");
            hwService.DownloadFile(FileIdDownload,Download,FileDownloadName);
        } else if (op == 4) {
            for(FilesModel infos : seleccionados){
                FilesModel dato = new FilesModel();
                dato.setFilename(infos.getFilename());
                File Download = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(),  infos.getFilename());
                dato.setArchivo(Download);
                dato.setFileId(infos.getFileId());
                multis.add(dato);
            }
            progressContentMultiple.setVisibility(View.VISIBLE);
            textInfoMultiple.setText("Procesando archivos...");
            progreso_multiple.setIndeterminate(true);
            messageApp.LoadToast("Eliminando "+ multis.size() + " archivos espere por favor");
            hwService.DeleteMultiplesFiles(multis);
        } else if (op == 5) {
            for(FilesModel infos : seleccionados){
                FilesModel dato = new FilesModel();
                dato.setFilename(infos.getFilename());
                File Download = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(),  infos.getFilename());
                dato.setArchivo(Download);
                dato.setFileId(infos.getFileId());
                multis.add(dato);
            }
            progressContentMultiple.setVisibility(View.VISIBLE);
            textInfoMultiple.setText("Procesando archivos...");
            progreso_multiple.setIndeterminate(true);
            messageApp.LoadToast("Descargando "+ multis.size() + " archivos espere por favor");
            hwService.DownloadMultiplesFiles(multis);
        }
    }

    @Override
    public void Cancel() {

    }

    @Override
    public void CheckVersion() {
        RemoteData remoteData = utilsApp.obtenerDatos();
        if (remoteData.Version > actual) {
            messageApp.LoadMessage(remoteData.Titulo, remoteData.Mensaje, remoteData.Estado);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SIGN_IN_LOGIN) {
            Task<AuthHuaweiId> authHuaweiIdTask = HuaweiIdAuthManager.parseAuthResultFromIntent(data);
            if (authHuaweiIdTask.isSuccessful()) {
                AuthHuaweiId huaweiAccount = authHuaweiIdTask.getResult();
                accessToken = huaweiAccount.getAccessToken();
                unionId = huaweiAccount.getUnionId();
                int returnCode = init(unionId, accessToken);
                if (DriveCode.SUCCESS == returnCode) {
                    hwService.LoginSession();
                    messageApp.LoadToast("Bienvenido: " + huaweiAccount.getDisplayName());
                    ShowForms(View.VISIBLE);
                    showOptions(true);
                    username.setText(huaweiAccount.getDisplayName());
                    if (huaweiAccount.getAvatarUriString() != null) {
                        imagesUtils.LoadImagePath(foto, huaweiAccount.getAvatarUriString());
                    }
                    infoUser.setVisibility(View.VISIBLE);
                } else if (DriveCode.SERVICE_URL_NOT_ENABLED == returnCode) {
                    messageApp.LoadToast("Huawei Drive no está disponible");
                    infoUser.setVisibility(View.GONE);
                } else {
                    messageApp.LoadToast("login error");
                    infoUser.setVisibility(View.GONE);
                }
                login.setVisibility(View.GONE);
            } else {
                messageApp.LoadToast("Debe iniciar sesión");
                infoUser.setVisibility(View.GONE);
                login.setVisibility(View.VISIBLE);
            }
        } if (requestCode == REQUEST_SELECT_FILE) {
            if (resultCode == RESULT_OK) {
                String Fpath = getFileName(data.getData());
                selectFile.setText(Fpath);
                archivo = new File(FileUtils.getRealPath(this, data.getData()));
            }
        }
        if (requestCode == REQUEST_SELECT_FILES) {
            if (resultCode == RESULT_OK) {
                try {
                    if (data.getData() != null) {
                        File archivoN = new File(FileUtils.getRealPath(this, data.getData()));
                        archivos.add(archivoN);
                        String Fpath = getFileName(data.getData());
                        selectMultipleFile.setText(Fpath);
                    } else if (data.getClipData() != null) {
                        ClipData clipData = data.getClipData();
                        if(clipData.getItemCount() > 0) {
                            for (int count = 0; count < clipData.getItemCount(); count++) {
                                Uri uri = clipData.getItemAt(count).getUri();
                                File archivoN = new File(FileUtils.getRealPath(this, uri));
                                archivos.add(archivoN);
                            }
                            selectMultipleFile.setText(clipData.getItemCount() + " archivos seleccionados");
                        } else{
                            Uri uri = clipData.getItemAt(0).getUri();
                            File archivoN = new File(FileUtils.getRealPath(this, uri));
                            archivos.add(archivoN);
                            selectMultipleFile.setText(clipData.getItemCount() + " archivos seleccionados");
                        }
                    }
                } catch (Exception e){
                    messageApp.LoadToast(e.getMessage());
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login) {
            IniciarSesion();
        } if (v.getId() == R.id.close) {
            CerrarSesion();
        } if (v.getId() == R.id.op1) {
            ShowOtherForms(0);
        } if (v.getId() == R.id.op2) {
            ShowOtherForms(1);
        } if (v.getId() == R.id.searchFolder) {
            String name = nameSearchFolder.getText().toString().trim();
            if (name.equals("")) {
                messageApp.LoadToast("Debe ingresar el nombre de la carpeta que desea buscar");
            } else {
                hwService.SearchFolder(name);
            }
        } if (v.getId() == R.id.createSub) {
            String name = subFolderName.getText().toString().trim();
            if (name.equals("")) {
                messageApp.LoadToast("Debe ingresar el nombre de la carpeta que desea crear");
            } else {
                hwService.CreateSubFolder(name, FolderIdSearch);
            }
        } if (v.getId() == R.id.backSub) {
            ShowOtherForms(3);
        } if (v.getId() == R.id.op3) {
            ShowOtherForms(2);
        } if (v.getId() == R.id.backFolder) {
            ShowOtherForms(3);
        } if (v.getId() == R.id.create) {
            String name = nameFolder.getText().toString().trim();
            if (name.equals("")) {
                messageApp.LoadToast("Debe ingresar el nombre de la carpeta que desea crear");
            } else {
                hwService.CreateFolderIfNotExist(name);
            }
        }  if (v.getId() == R.id.selectFile) {
            if(!checkPermission() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermission();
            } else{
                OpenFiles();
            }
        }  if (v.getId() == R.id.selectMultipleFile) {
            if(!checkPermission() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermission();
            } else{
                OpenFilesMultiple();
            }
        } if (v.getId() == R.id.uploadFile) {
            if(FolderId.equals("")){
                messageApp.LoadToast("No se ha creado ningún folder principal para usar");
            }  else if(archivo != null){
                messageApp.LoadToast("Subiendo archivo, espere un momento por favor");
                hwService.UploadFile(archivo,FolderId);
            } else{
                messageApp.LoadToast("Aun no se ha cargado el archivo");
            }
        }  if (v.getId() == R.id.uploadMultipleFile) {
            if(FolderId.equals("")){
                messageApp.LoadToast("No se ha creado ningún folder principal para usar");
            }  else if(archivos != null && archivos.size() > 0){
                progressContent.setVisibility(View.VISIBLE);
                progreso.setIndeterminate(true);
                hwService.UploadMultiplesFiles(archivos,FolderId);
            } else{
                messageApp.LoadToast("Aun no se han cargado los archivos");
            }
        } if (v.getId() == R.id.backFiles) {
            ShowOtherForms(3);
        } if (v.getId() == R.id.listFiles) {
            if(FolderId.equals("")){
                messageApp.LoadToast("No existe un folder principal para cargar");
            } else{
                cargas.setVisibility(View.VISIBLE);
                hwService.ListFolder(FolderId);
            }
        } if (v.getId() == R.id.Download) {
            if(seleccionados.size() <= 0){
                messageApp.LoadToast("No hay archivos seleccionados para descargar");
            } else{
                messageApp.DownloadMultipleFile("Descarga multiple","Estás a punto de descargar "+seleccionados.size() +" archivos, \n ¿Deseas continuar con la descarga?");
            }
        } if (v.getId() == R.id.Delete) {
            if(seleccionados.size() <= 0){
                messageApp.LoadToast("No hay archivos seleccionados para eliminar");
            } else{
                messageApp.DeleteMultipleFile("Eliminación múltiple","Estás a punto de eliminar "+seleccionados.size() +" archivos, \n ¿Deseas continuar con la eliminación de los archivos seleccionados?");
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            try (Cursor cursor = getContentResolver().query(uri, null, null, null, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public void HDriveSession(boolean loggin, AuthHuaweiId authHuaweiId) {
        if (loggin) {
            infoUser.setVisibility(View.VISIBLE);
            login.setVisibility(View.GONE);
            functions.setVisibility(View.VISIBLE);
            username.setText(authHuaweiId.getDisplayName());
            ShowForms(View.VISIBLE);
            if (authHuaweiId.getAvatarUriString() != null) {
                imagesUtils.LoadImagePath(foto, authHuaweiId.getAvatarUriString());
            }
        } else {
            infoUser.setVisibility(View.GONE);
            login.setVisibility(View.VISIBLE);
            functions.setVisibility(View.GONE);
            ShowOtherForms(3);
        }
    }

    @Override
    public void HDCloseSession() {
        infoUser.setVisibility(View.GONE);
        login.setVisibility(View.VISIBLE);
        functions.setVisibility(View.GONE);
        ShowOtherForms(3);
        showOptions(false);
        messageApp.LoadToast("La sesión se ha cerrado correctamente");
    }

    @Override
    public void ErrorHDrive(String Message) {
        messageApp.LoadToast(Message);
    }

    @Override
    public void FolderCreate(String IdFolder, String folder) {
        FolderId = IdFolder;
        sharedData.SaveData(LAST_FOLDER_ID,FolderId,false,0,0);
        nameFolder.clearFocus();
        messageApp.LoadToast("El folder " + folder + " se ha creado correctamente com el siguiente ID: " + IdFolder);
        nameFolder.setText("");
    }

    @Override
    public void SubFolderCreate(String foldersId, String folder) {
        subFolderName.setText("");
        subFolderName.clearFocus();
        messageApp.LoadToast("El folder " + folder + " ha sido creado con el siguiente ID: " + foldersId);
    }

    @Override
    public void FolderSearch(String foldersId, String folder) {
        if (foldersId.equals("")) {
            FolderIdSearch = "";
            messageApp.LoadToast("El folder " + folder + " no existe en su cuenta de Huawei Drive");
            idSearchFolder.setText("Esperando encontrar un folder");
            subFolderName.clearFocus();
            nameSearchFolder.clearFocus();
            foundDataFolder.setVisibility(View.GONE);
        } else {
            FolderIdSearch = foldersId;
            idSearchFolder.setText(String.format(getString(R.string.Folder_id), foldersId));
            subFolderName.clearFocus();
            nameSearchFolder.clearFocus();
            messageApp.LoadToast("El folder " + folder + " ha sido encontrado com el siguiente ID: " + foldersId);
            foundDataFolder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void SuccessUpload(boolean upload) {
        if(upload){
            archivo = null;
            archivos.clear();
            selectFile.setText("");
            messageApp.LoadToast("El archivo se ha subido correctamente");
        } else{
            messageApp.LoadToast("Hubo un error al subir el archivo");
        }
        progressContent.setVisibility(View.GONE);
    }

    @Override
    public void UploadProgress(int progress, int count, int total) {
        new Handler().post(() -> {
            progreso.setIndeterminate(false);
            progreso.setProgress(progress);
            textoInfo.setText("Subiendo "+ count +" de "+total);
        });
        if(count == total){
            messageApp.LoadToast("Todos los archivos se han subido correctamente");
            progressContent.setVisibility(View.GONE);
            selectMultipleFile.setText("");
            archivo = null;
            archivos.clear();
            hwService.ListFolder(FolderId);
        }
    }

    @Override
    public void ListFolder(JSONArray response) {
        try {
            files.clear();
            if(response.length() > 0) {
                for (int i = 0; i < response.length(); i++) {
                    JSONObject json_data = response.getJSONObject(i);
                    if (!json_data.getString("id").equals("")) {
                        FilesModel filesModel = new FilesModel();
                        filesModel.setFileId(json_data.getString("id"));
                        filesModel.setFilename(json_data.getString("fileName"));
                        filesModel.setFiletype(json_data.getString("mimeType"));
                        filesModel.setPosition(i);
                        filesModel.setSelect(false);
                        files.add(filesModel);
                    }
                }
                multiples.setVisibility(View.GONE);
                multis.clear();
                seleccionados.clear();
                adapter.notifyDataSetChanged();
            } else{
                messageApp.LoadToast("No existen archivos en el folder");
            }
            cargas.setVisibility(View.GONE);
        } catch (Exception e){
            messageApp.LoadToast("Error: "+e.getMessage());
        }
    }

    @Override
    public void ListAllFiles(JSONArray response) {
        try {
            files.clear();
            if(response.length() > 0) {
                for (int i = 0; i < response.length(); i++) {
                    JSONObject json_data = response.getJSONObject(i);
                    if (!json_data.getString("id").equals("")) {
                        FilesModel filesModel = new FilesModel();
                        filesModel.setFileId(json_data.getString("id"));
                        filesModel.setFilename(json_data.getString("fileName"));
                        filesModel.setFiletype(json_data.getString("mimeType"));
                        filesModel.setPosition(i);
                        filesModel.setSelect(false);
                        files.add(filesModel);
                    }
                }
                multiples.setVisibility(View.GONE);
                multis.clear();
                seleccionados.clear();
                adapter.notifyDataSetChanged();
            } else{
                messageApp.LoadToast("No existen archivos en el folder");
            }
            cargas.setVisibility(View.GONE);
        } catch (Exception e){
            messageApp.LoadToast("Error: "+e.getMessage());
        }
    }

    @Override
    public void DeleteSuccess(boolean state, String file) {
        if(state){
            files.remove(posicionDelete);
            adapter.notifyDataSetChanged();
            messageApp.LoadToast("Archivo "+file+" eliminado correctamente");
        }
    }

    @Override
    public void DownloadSuccess(boolean state, String file) {
        if(state){
            messageApp.LoadToast("El archivo "+file +" se ha descargado correctamente en la carpeta Descargas");
        }
    }

    @Override
    public void MultipleProgress(int progress, int count, int total, boolean type) {
        new Handler().post(() -> {
            progreso_multiple.setIndeterminate(false);
            progreso_multiple.setProgress(progress);
            if(type){
                textInfoMultiple.setText("Descargando "+ count +" de "+total);
            }
            else{
                textInfoMultiple.setText("Eliminando "+ count +" de "+total);
            }

        });
        if(count == total){
            if(type){
                messageApp.LoadToast("Todos los archivos se han descargado correctamente");
            }
            else{
                messageApp.LoadToast("Todos los archivos se han eliminado correctamente");
            }
            multis.clear();
            seleccionados.clear();
            hwService.ListFolder(FolderId);
            progressContentMultiple.setVisibility(View.GONE);
        }
    }

    @Override
    public void DeleteFile(String name, String FileId,int op) {
        FileIdDelete = FileId;
        posicionDelete = op;
        FileDeleteName = name;
        messageApp.DeleteFile("Eliminar archivo","Estas a punto de eliminar "+name+" \n ¿Desea continuar y eliminar el archivo de Huawei Drive?");
    }

    @Override
    public void Download(String name, String FileId,int op) {
        FileIdDownload = FileId;
        FileDownloadName = name;
        messageApp.DownloadFile("Descargar archivo","Estas a punto de descargar "+name+" \n ¿Desea continuar y descargar el archivo de Huawei Drive?");
    }

    @Override
    public void SelectMultiplesFiles(FilesModel filesModel) {
        seleccionados.add(filesModel);
        adapter.notifyItemChanged(filesModel.getPosition(),filesModel);
        multiples.setVisibility(View.VISIBLE);
    }

    @Override
    public void RemoveMultiplesFiles(FilesModel filesModel) {
        if(seleccionados.size() == 1){
            seleccionados.remove(filesModel);
            adapter.ReloadSelect();
            multiples.setVisibility(View.GONE);
        } else if(seleccionados.size() > 1) {
            seleccionados.remove(filesModel);
            adapter.notifyItemChanged(filesModel.getPosition(), filesModel);
        }
    }

    @Override
    public void MessageAction(String message) {
        messageApp.LoadToast(message);
    }
}
